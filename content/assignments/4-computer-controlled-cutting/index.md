+++
title = " 4 - Computer Controlled Cutting"
lead = "Vinyl cutting and laser cutting"
+++

# 4. Computer Controlled Cutting

The assignment for this week was to learn the basics of computer controlled cutting using a vinyl cutter and a laser cutter. The assignments for this week were:

- Group assignment
   - characterize your lasercutter's focus, power, speed, rate, kerf, and joint clearance
   - document your work (individually or in group)

- Individual assignments
  - Design, lasercut, and document a parametric press-fit construction kit, which can be assembled in multiple ways. Account for the lasercutter kerf.
  - cut something on the vinylcutter

The desired learning outcomes for the week were to:

- Demonstrate and describe parametric 2D modelling processes
- Identify and explain processes involved in using the laser cutter.
- Develop, evaluate and construct the parametric construction kit
- Identify and explain processes involved in using the vinyl cutter.
Have you

To discuss the work completed this week I will share first the group assignment, and then the individual assignment.

## Group Assignment

The objective of the group assignment was to characterize our lasercutter's focus, power, speed, rate, kerf, and joint clearance and document our work.

Before beginning with the exercise. Our Fab Node Instructor Jason went through a detailed overview of how to use the laser cutter. His instructions are included in the video below (warning, its long):

{{<youtube 4U-nYf-Ev-c>}}

After learning how to operate the laser cutter and cutting an hp logo out of cardboard, we then moved on to characterising the focus, power and speed. To accomplish this, I selected a project in which I was to rastor an photo of my wife and I onto a piece of wood. We first started with comparing the type of rastor, between jarvis and ordered using a test piece of plywood. The result is illustrated below:

![Jarvis vs Ordered](orderedVSjarvis.jpg)

Next we looked at the effect of speed, focus, and power. These are illustrated in the photos below in which: 

P = Power
S = Speed
DF = Defocus height (approx)

![Speed and Power](PowerSettings.jpg)
![Defocus] (defocus.jpg)
![Jarvis 15s](Jarvis15s.jpg)

After having investigating the various settings and characterising the correct settings for this piece, the final values were determined as the right values for the project. The values are given below:

P = 100 %
S = 12 %
DF = 0 mm
Style = jarvis

The final result of the characterisation is illustrated in the photo below:

![Final](Final.jpg)

below is a video describing the 2D modeling methods that were used:

{{<youtube ZZ2d2JubsEQ>}}


## Individual Assignments

This week there were two individual assignments, the first was to cut something using the vinyl cutter. For this assignment I elected to cut some stickers for my laptop. The second assignment was to Design, lasercut, and document a construction kit which can be assembled in multiple ways. The kerf of our laser is about 1 mm.

I will present this section as using the vinyl cutter first, and the laser cutter with parametric design second.

### Vinyl cutter

For this assignment, I elected to do a simple project that could also add some flare to my boring old laptop. To do this assignment I first had to learn how to use the vinyl cutter with Jason, and then I designed and cut the Energy Generation logo (organisation that I work for) and an hp logo. That process is documented in its entirety in the video below:

{{<youtube IxG1kEPhHyM>}}


### Laser cutter

For this assignment I elected to practice laser cutting a piece for my final project that I will eventually want to mill with a CNC out of sheet metal. I wanted to practice taking a circular object, and flattening it out so that you can cut holes in it before bending and riveting it together. To test this out, I made a parametric design of the combustion chamber internal tube shown in the image below.

![assembly exploded view](AssemblyExploded.jpg)

To make the model parameterized I had to perfom several calculations using the FreeCad Spreadsheets workbench combined with defining/constraining certain variables using equations. I did an in depth look at how to create a parameterized CAD file in Assignment 3 - Computer Aided Design. 

For this week, I chose to focus my time on demonstrating how to take a parameterised sketch made in FreeCad, and prepare it for cutting with the laser. The video below demonstrates how to do this:

{{<youtube wrCrWh0J-10>}}

Once the .eps file had been created and imported into the computer that controls the laser cutter, it was simply a matter of cutting it and doing the final preparation. The video below documents that process.

{{<youtube W_NxHHXcWQ8>}}


### Press fit components

Following the completion of the rastoring and cutting exercise for my project. I also completed the laser cutting of components that could be cut and assembled using press fits. I chose to make a christmas ornament in the shape of a snowflake. I started by drawing the image using InkScape to produce the following result.

![Star first iteration](../images/week04/PressFit.jpg)

![Star first iteration assembled](../images/week04/PressFit3.jpg)

after laser cutting, I noticed that the size did not fit well, as the width of slit did not match up with the width of the cardboard. I therefore decided to create a test fit piece and rescale the snowflake to ensure a better press fit. The test fit pieces were simple rectangles with different widths cut out of them. The results of these two pieces were as follows:

![Press fit test](PressFitTest2.jpg)

![Press fit test assembled](PressFitTest3.jpg)


Based on the press fit test pieces, it was determine that a width of 3.5 mm was ideal. I therefore used this to rescale the parameterized snowflake design, using the slit width as the driving variable. The results of the new design are shown below:

![Press fit small](PressFitSmall.jpg)

![Press fit small assembled](PressFitSmall2.jpg)

The result was MUCH BETTER! and I learned how to adequately test and size my press fits for future projects. Lastly, I took a photo of all of the pieces I cut to show the difference and the progress.

![Press fit final](PressFitFinal.jpg)

