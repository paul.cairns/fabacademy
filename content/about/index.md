+++
title = "About me"
lead = "An engineer and humanitarian working in Africa to help locals develop local solutions to local problems"
+++

{{<image-responsive src="Paul-Cairns.jpg" alt="Paul Cairns">}}

Hello, My name is Paul Cairns, I have a Bachelor’s in Mechanical Engineering, and a Master’s in Chemical Engineering. After graduating I spent six years of my professional career as a Research Engineer for the Government of Canada. I also spent two years as Executive Director for a green energy business incubator based out of Ottawa, Canada. Just over a year ago I joined Energy Generation where I manage the EnergyTech Department and EnergyLab, our FabLab based out of Lomé, Togo. More about that below.

## My background

I was born in the quiet and small town of Cobourg, Ontario, Canada. Its claim to fame is its sandy beach on Lake Ontario. I went to the Cobourg highschool where I played on the rugby team, and was deeply involved with Scouts and Sea Cadets. It was through Sea Cadets that I learned how to sail. Following my graduation from highschool, I was accepted to the University of Ottawa's Mechanical Engineering Department where I obtained a B.A.Sc. in Mechanical Engineering COOP, with Management Option. It was through my COOP placements that I got a job at Natural Resources Canada as a Research Engineer where I completed my thesis research for a Master's in Chemical Engineering. My research revolved around reducing the carbon emisisons of Oil Sands extraction. 

![Achievement Award](PaulNRCanDepartmentalAchievement.jpg)

After several years working as a Research Engineer, I realized that I did not want to continue my efforts working on a bandaid solution to climate change and decided to pivot my career towards renewables in the form of Net-zero homes. To do this I founded a not-for-profit organisation that advocated for net-zero homes and learned about passive homes, small-scale solar systems, and expanded my competencies. After two years working on the not-for-profit, I joined an green energy business incubator as Executive Director. I worked as Executive Director for two years before deciding to take a one year sabbatical and travel the world.

{{<youtube 6K42PvX3tT0>}}


{{<youtube 5WyFo99u_Kg>}}


{{<youtube pc5Y5nlscCQ>}}


During my travels I got into contact with Engineers Without Borders and found the position as Technical Director at Energy Generation (EG). After a year of volunteering with EG, I joined full-time and am now joining the FabAcademy to work towards obtaining the credentials necessary to become a Fab Academy Node in Togo.

![Laser Cutter](InstallLaserCutter1.jpg)

## Previous work

In this section I will discuss previous projects in which I have been involved and a contributor towards

### HiPrOx DCSG

My original research at Natural Resources Canada (NRCan) involved High Pressure Oxy-fired Direct Contact Steam Generation for Oil sands extraction. Over my tenure at NRCan, I worked on designing and building a 100 atm pilot-plant. It was through this work that I learned a lot about chemical engineering processes, controls, and research. 

![Research](PaulCairnsHardHatNRCan.jpg) 
![DCSG Plant](DCSGPlant.jpg)

### Net-zero Home

My next work revolved around net-zero homes, in which I bought a bungalow and converted into a duplex while also increasing the energy efficiency of the home. I was unable to achieve net-zero, but I was able to increase the energy efficiency of the building by around 30%

{{<youtube _7IaFQZ9NGg>}}

{{<youtube ZxKwfJRep0E>}}


### Energy Genration

Through my work with Energy Generation, I have helped build our FabLab, help build our CNC machine, Weld and construct our tables, run our Arduino Club, and built various prototypes for local companies

{{<image-matrix-3 InstallLaserCutter2.jpg LaserCutter.jpg Welding.jpg>}}

{{<image-matrix-3 Soldering.jpg ToolBoard.jpg CNC.jpg>}}

{{<image-matrix-3 ArduinoClub.jpg ArduinoClub2.jpg SolarTraining.jpg>}}

Here is a video of one of the prototypes we built for a local company. It is a mechanical seeder.

{{<youtube -VMzWnFxxnk>}}
s
